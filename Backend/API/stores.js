var	mysql = require('../core_modules/mysql.js'),
	   Q  = require('../node_modules/q'),
	utils = require('../core_modules/utils.js'),
	http  = require('http');

/**
 *  Stores.js
 *  Entry points for user operations
 *  COVERS: SearchStore, listStores
 */


// Get a connection to the ShopLift database
var shopliftDB = mysql.getConnection("localhost", "ShopLift");

 module.exports = {

 	/** SEARCH STORE **/
 	searchStore: function() {
 		var name = utils.urldecode(params.name);

 		mysql.select(shopliftDB, "SELECT Store_ID FROM Stores"
 					+ " WHERE Name='"+name+"'")
 		.then(function(value) {
 			if (utils.objectIsEmpty(value)) {
 				console.log("Store not found");
 				output = {
 					result: false,
 					Store_ID : -1,
 					error: "Store not found"
 				};
 			}
 			else {
 				console.log("Store found yooo");
 				output = {
 					result : true,
 					Store_ID: value[0].Store_ID
 				}
 			}

 			utils.respond(output);
 		})
 		.fail(function(error) {
 			console.log(error);
 			utils.respond({
 				error: "Error in search of store"
 			})
 		});

 	},


 	/*  Search a product based on Store and barcode */
 	searchProduct: function() {
 		var store_id = params.store_id;
 		var barcode = params.barcode;
 		var connInfo = utils.parseConnectionInfo(params.connInfo);

 		// Get connection to the store's DB
 		var storeDB = mysql.getConnection(connInfo.host, connInfo.db);

 		// Query something from there
 		mysql.select(storeDB, "SELECT * from products")
 		.then(function(value) {
 			utils.respond({
 				Product_ID: value[0].Product_ID,
 				name: value[0].Name,
 				price: value[0].Price
 			})
 		})
 		.fail(function(error) {
 			console.log(error);
 			utils.respond({
 				error: "Error in search of product"
 			})
 		});
 	}
 }