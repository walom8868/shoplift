var	mysql = require('../core_modules/mysql.js'),
	   Q  = require('../node_modules/q'),
	utils = require('../core_modules/utils.js'),
	http  = require('http');

/**
 *  Users.js
 *  Entry points for user operations
 *  COVERS: Login, Logout, Signup, AlterUser
 */

// Get a connection to the ShopLift database
var shopliftDB = mysql.getConnection("localhost", "ShopLift");

module.exports = {

	/** LOGIN  **/
	login: function () {
		var username = params.username;
		var password = params.password;
		console.log(username + " - " + password);
		// Retrieve data from DB
		var queried = 
			mysql.select(shopliftDB, "SELECT User_ID, Password FROM Users"
						+ " WHERE Username='" + username + "'");

		queried.then(function(value) {
			// If result is empty, means username doesn't exists
			if (utils.objectIsEmpty(value)) {
				console.log("Username doesn't exists");
				output = {
					valid: false,
					User_ID: -1,
					error: "Username does not exists"
				};
			}
			// Else, compare passwords
			else if (password != value[0].Password) {
				console.log("Password doesn't match! - " + value[0].Password + "uio");
				output = {
					valid: false,
					User_ID: -1,
					error: "Password is incorrect"
				};
			}
			else {
				console.log("good~~~");
				output = {
					valid: true,
					User_ID: value[0].User_ID
				};
			}

			utils.respond(output);
		})
		.fail(function(error) {
			console.log("Error occurred");
			console.log(error);
		});
	},


	/** LOGOUT  **/
	logout: function() {
		utils.respond({
			valid: false,
			output: "Logout not yet supported"
		});
		
		console.log("Logout requested");
	},


	/** SIGNUP  **/
	signup: function() {
		if (utils.objectIsEmpty(params)) {
			console.log("Need to pass username and password");
			utils.respond({
				result: false,
				User_ID: -1,
				error: "Need to pass username/password"
			});
		}

		else {
			// Need to get username and password
			var data = {
				username: params.username,
				password: params.password
			}
			console.log(JSON.stringify(data));

			// Insert the thing and send reply
			mysql.insert(shopliftDB, "Users", data)
			.then(function(value) {
				// 
				utils.respond({
					result: true,
					User_ID: value.insertId	
				});
			})
			.fail(function(error) {
				console.log(error);
			});
		}
	},


	/** ALTERUSER  **/
	alteruser: function() {
		
	}
}