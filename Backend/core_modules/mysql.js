var mysql = require('../node_modules/mysql'),
	Q  = require('../node_modules/q'),
	utils = require('./utils.js');

module.exports = {

	getConnection: function(host, db) {
		console.log(db);
		var con = mysql.createConnection({
			host: host,
			// User shoplift hardcoded because it already has privileges for everything
			user: "shoplift",
			database: db
		});

		con.connect(function(err) {
			if (err) {
				console.log("Error connecting to the DB");
				return;
			}
		});

		return con;
	},

	insert: function(con, table, data) {
		var deferred = Q.defer();

		con.query("INSERT INTO " + table + " SET ?", data, function(err, res) {
			if (err)
				deferred.reject(new Error(err));
			else
				deferred.resolve(res);
		});

		return deferred.promise;
	},

	select: function(con, query) {
		var deferred = Q.defer();

		con.query(query, function(err, rows) {
			if (err) 
				deferred.reject(new Error(err));
			else
				deferred.resolve(rows);
		});

		return deferred.promise;
	},



	update: function(con, table, data, filter) {
		// Build data in sql syntax
		values = "";
		for (column in data) 
			values += column + "=" + data[column] + ", ";

		values = values.substring(0, values.length - 2);
		
		var query = "UPDATE " + table + " SET " + values;
		if (filter != undefined && filter != "");
			query += " WHERE " + filter;0

		var deferred = Q.defer();

		con.query(query, function(err, result) {
			if (err)
				deferred.reject(new Error(err));
			else
				deferred.resolve(result);
		});

		return deferred.promise;
	},


	delete: function(con, table, filter) {
		var deferred = Q.defer();

		con.query("DELETE FROM " + table + " WHERE " + filter, function(err, result) {
			if (err)
				deferred.reject(new Error(err));
			else
				deferred.resolve(result);
		});

		return deferred.promise;
	},

	close: function(con) {
		con.end(function(err) {
			;
		});
	}
}