module.exports = {
	objectIsEmpty: function(obj) {
	    // null and undefined are "empty"
	    if (obj == null) return true;

	    // Assume if it has a length property with a non-zero value
	    // that that property is correct.
	    if (obj.length > 0)    return false;
	    if (obj.length === 0)  return true;

	    // Otherwise, does it have any properties of its own?
	    // Note that this doesn't handle
	    // toString and valueOf enumeration bugs in IE < 9
	    for (var key in obj) {
	        if (Object.prototype.hasOwnProperty.call(obj, key)) return false;
	    }

	    return true;
	},


	// Write and send response to client
	respond: function(response_obj) {
		// Write back to the requestor as JSON
		response.write(JSON.stringify(response_obj));
		response.end();
	},
	
	// Decode URL-encoded special characters
	urldecode: function(encoded_string) {
		var decoded = encoded_string.replace("%27", "\\'");
		console.log(encoded_string);
		console.log(decoded);
		return decoded;
	},

	// Break down connection strings for each Store's DB
	// Has the format <host/IP address>-<DB name>
	parseConnectionInfo: function(connectionInfo) {
		var info = connectionInfo.split('-');
		return {
			host: info[0],
			db: info[1]
		};
	}
}