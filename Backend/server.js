var http = require('http'),
	users = require('./API/users.js'),
	stores = require('./API/stores.js'),
	utils = require('./core_modules/utils.js');


/**
 *  Main Wrapper function to route incoming HTTP requests
 */ 

function route(request) {
	var url = request.url;

	if (url == "/favicon.ico") return;

	if (request.method == 'GET') {
		url = url.split('?');
		params = {};

		console.log("URL: " + url);
		
		// Break down the url -> Extract the parameters
		// Here, raw_params is an array that looks like "key=value"
		if (url[1] != "" && url[1] != undefined) {
			var raw_params = url[1].split('&');

			// Params is a usable object of params passed { key : value }
			var tmp;
			for (var i = 0; i < raw_params.length; i++) {
				tmp = raw_params[i].split('=');
				params[tmp[0]] = tmp[1];
			}
		}

		url[0] = url[0].substring(1);
		// Operations
		switch (url[0]) {
			// User related operations
			case 'login':
			case 'logout':
			case 'signup':
				users[url[0]]();
				break;


			// Store-Product related operations
			case 'searchStore':
			case 'searchProduct':
				stores[url[0]]();
				break;

			default:
				utils.respond({ error: "Unsupported operation"});
		}
	}
}


/*
 *  Start up the server
 */
http.createServer(function (request, local_response) {
	// Make response global
	response = local_response;

	// All responses in JSON
	response.writeHead(200, {'Content-Type': 'application/json'});

	// Route the request
	route(request);
}).listen(8079);