package com.example.xsl.shoplift;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by XSL on 11/30/15.
 */
public class Database {

    public static final String IP = "45.55.47.173";
    public static final int PORT = 8079;

    public static JSONObject basicCall(String type, String operation, String... parameters) throws Exception {
        JSONObject result = null;

        // Build url to use
        String url = "http://"+ Database.IP + ":" + Database.PORT + "/" + operation + "?";
        int line = 0;
        for (String param_element : parameters) {
            url += param_element;
            url += (line % 2 == 0) ? "=" : "&";
        }

        url = url.substring(0, url.length() - 1);

        Database.AsyncBackendTask task = new Database.AsyncBackendTask();

        // Execute http request
        task.execute(url);

        // Check until it's done
        while (!task.done)
            Thread.sleep(100);

        // When done, get the result
        return new JSONObject(task.extResult);
    }

    public static JSONObject get(String operation, String... urls) {
        return null;
    }

    private static class AsyncBackendTask extends AsyncTask<String, Void, String> {
        public String extResult;
        public boolean done;

        public AsyncBackendTask() {
            this.extResult = "";
            this.done = false;
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                String result = downloadUrl(urls[0]);
                this.extResult = result;
                this.done = true;
                return extResult;
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private String downloadUrl(String tmp) throws Exception {

            URL url  = new URL(tmp);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");


            byte[] response = getByteArray(connection);


            Log.d("network", "running");
            return new String(response);
        }


        public byte[] getByteArray(HttpURLConnection connection) throws IOException {

            InputStream is = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[10000];  	//TODO: Is it sufficiently enough of memory?

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        }
    }
}
