package com.example.xsl.shoplift;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Login extends Activity {

    EditText UserNameTv;
    EditText PasswordTv;
    Button SubmitButton;
    TextView ResultTv;
    String UserName;
    String Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SubmitButton=(Button)findViewById(R.id.button);
        UserNameTv=(EditText)findViewById(R.id.userName);
        PasswordTv=(EditText)findViewById(R.id.password);
        ResultTv=(TextView) findViewById(R.id.result);
        SubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Submit(v);
                } catch (Exception e) {
                    Log.d("onclick",e.getMessage());
                    e.printStackTrace();
                }
            }
        });

    }

    public void Submit(View view) {
        String tmp = "http://45.55.47.173:8079/login?";

        //TEST
        UserName=UserNameTv.getText().toString();
        Password=PasswordTv.getText().toString();


        // Add username
        tmp += "username=" + UserName;

        // Add password
        tmp += "&password=" + Password;

        try {
            JSONObject json_response = Database.basicCall("GET", "login", "username", UserName, "password", Password);
            Boolean valid_login = json_response.getBoolean("valid");
            int user_id = json_response.getInt("User_ID");
            ResultTv.setText("valid: " + valid_login + " - User_ID: " + user_id);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //(new TestLoginAsync()).execute(tmp);

    }

    public void Register(View view) {

    }


    private class TestLoginAsync extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadUrl(urls[0]);
            }
            catch (Exception e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                ResultTv.setText(result);
                JSONObject json_response = new JSONObject(result);
                Boolean valid_login = json_response.getBoolean("valid");
                int user_id = json_response.getInt("User_ID");
                ResultTv.setText("valid: " + valid_login + " - User_ID: " + user_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String downloadUrl(String tmp) throws Exception {

            URL url  = new URL(tmp);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");


            byte[] response = getByteArray(connection);


            Log.d("network", "running");
            return new String(response);
        }


        public byte[] getByteArray(HttpURLConnection connection) throws IOException{

            InputStream is = connection.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[10000];  	//TODO: Is it sufficiently enough of memory?

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_testing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
